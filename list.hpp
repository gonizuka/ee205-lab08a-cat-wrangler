///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.hpp
/// @version 1.0
///
/// 
/// @author Gavin Onizuka <gonizuka@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   @04_11_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include <cassert>
#include "node.hpp"

using namespace std;

   class DoubleLinkedList{
      protected:
         Node* head = nullptr;
         Node* tail = nullptr;
         unsigned int count = 0;
      public:
         const bool empty() const;
         void push_front(Node* newNode);
         Node* pop_front();
         Node* get_first() const;
         Node* get_next( const Node* currentNode ) const;
         unsigned int size() const;

         void push_back( Node* newNode);
         Node* pop_back();
         Node* get_last() const;
         Node* get_prev( const Node* currentNode) const;
         const bool isIn( Node* aNode) const;
         bool isBefore(Node* a, Node* b);
         void swap(Node* node1, Node* node2);
         void insertionSort();
   };


