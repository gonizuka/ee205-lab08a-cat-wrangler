///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.cpp
/// @version 1.0
///
///
/// @author Gavin Onizuka <gonizuka@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   @04_11_2021
///////////////////////////////////////////////////////////////////////////////

#include<iostream>
#include<cstdlib>
#include <cassert>
#include "list.hpp"
#include "node.hpp"

   const bool DoubleLinkedList::empty() const{
      return head == NULL;
   }

   void DoubleLinkedList::push_front( Node* newNode ){
      if(newNode == nullptr)
         return;

      if(head != nullptr){
         newNode->next = head;
         newNode->prev = nullptr;
         head->prev = newNode;
         head = newNode;
      }
      else{
         newNode->next = nullptr;
         newNode->prev = nullptr;
         head = newNode;
         tail = newNode;
      }
      count++;
   }

Node* DoubleLinkedList::pop_front(){
      if (head == NULL){
         return nullptr;
      }
      Node* temp = head;

      if(head == tail){
         head = nullptr;;
         tail = nullptr;;
         count--;
         return temp;
      }

      head = head -> next;
      head-> prev = nullptr;
      count--;

      return temp;
   }


   Node* DoubleLinkedList::get_first() const{
      return head;
   }

   Node* DoubleLinkedList::get_next( const Node* currentNode) const{
      return currentNode->next;
   }

   unsigned int DoubleLinkedList::size() const{
      return count;
   }

void DoubleLinkedList::push_back( Node* newNode){
   if(newNode == nullptr)
      return;

   if(tail != nullptr){
      newNode->next = nullptr;
      newNode->prev = tail;
      tail->next = newNode;
      tail = newNode;
   }else{ 
      newNode->next = nullptr;
      newNode->prev = nullptr;
      head = newNode;
      tail = newNode;
   }
   
   count++;
   
   return;
}

  Node* DoubleLinkedList::pop_back(){
     if(head == NULL){
        return nullptr;
     }

     Node* temp = tail;

     if(head == tail){
        head = nullptr;
        tail = nullptr;
        count--;
        return temp;
     }

     tail = tail->prev;
     tail->next = nullptr;

     count--;
     return temp;
  }

  Node* DoubleLinkedList::get_last() const{
     return tail;
  }

  Node* DoubleLinkedList::get_prev( const Node* currentNode) const{
     return currentNode->prev;
  }

const bool DoubleLinkedList::isIn( Node* aNode ) const {
   Node* currentNode = head;

   while( currentNode != nullptr){
      if( aNode == currentNode )
         return true;

      currentNode = currentNode->next;
   }

   return false; 
}


bool DoubleLinkedList::isBefore(Node* a, Node* b){
   Node* temp = head;

   while(temp != nullptr){
      if(a == temp){
         return true;
      }else if(b == temp){
         return false;
      }
      temp = temp->next;
   }
   return false;
}

void DoubleLinkedList::swap(Node* node1, Node* node2){

   assert(isIn(node1) && isIn(node2));

   if(node1==node2){
      return;
   }
   if(!isBefore(node1,node2)){
      Node* temp = node1;
      node1 = node2;
      node2 = temp;
   }

   Node* n1_left = node1->prev;
   Node* n1_right = node1->next;
   Node* n2_left = node2->prev;
   Node* n2_right = node2->next;

   bool isAdjacent = (n1_right == node2);

   if(!isAdjacent){
      node1->prev = node2->prev;
   }else{
      node1->prev = node2;
   }

   if(node1 != head){
      n1_left->next  = node2;
      node2->prev = n1_left;
   }
   else{
      node2->prev = nullptr;
      head = node2;
   }

   if(!isAdjacent){
      node2->next = n1_right;
   }else{
      node2->next = node1;
   }

   if(node2 != tail){
      n2_right->prev = node1;
      node1->next = n2_right;
   }else{
      node1->next = nullptr;
      tail = node1;
   }

   if(!isAdjacent){
      n1_right->prev = node2;
      n2_left->next = node1;
   }
}

void DoubleLinkedList::insertionSort(){
   if(count<=1){
      return;
   }
   for(Node* i = head->next; i != nullptr; i = i->next){
      for(Node* p = i->prev; p != nullptr; p = p->prev){
         if(*p > *i){
            swap(p,i);
         }
      }
   }

   return;
}


